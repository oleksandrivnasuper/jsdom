// Теоретичні питання
// 1.Опишіть своїми словами що таке Document Object Model (DOM)
//Це вся структура документа, що дає доступ до об'єктів і надає можливість їми володіти.
// 2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//Додуючи в строку валідний html текст, innerhtml його зчитує, створить та додасть ноди в DOM, а innerText - ні.  
// 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//elem.querySelector() - щоб знайти один елемент , elem.querySelectorAll() - щоб знайти всі єлемента за певною ознакою

// Завдання
// Код для завдань лежить в папці project.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

// Отримати елементи

// , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


let allParagrah = document.querySelectorAll('p');
for (let p of allParagrah) {
    p.style.backgroundColor = 'red';
}
console.log(allParagrah);


let idOptionList = document.querySelector('#optionsList');
console.log(idOptionList);
console.log('childNodes', idOptionList.childNodes);
console.log('nodeType', idOptionList.childNodes.nodeType);
// let childNodesID = idOptionList.childNodes;
// console.log(childNodesID);


let settleContent = document.querySelector('.testParagraph').innerHTML = 'This is a paragraph';
console.log(settleContent);


let headerTag = document.querySelector('.main-header').childNodes;
console.log(headerTag);
for (let node of headerTag) {
   node.className = 'nav-item';
}
console.log(headerTag);


let allSectionTitle = document.querySelectorAll('.section-title');
console.log(allSectionTitle);
for (let element of allSectionTitle){
    element.classList.remove('section-title');
}
console.log(allSectionTitle);


